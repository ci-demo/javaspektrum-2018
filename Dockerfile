FROM openjdk:8-jre-alpine
MAINTAINER Christine Koppelt <christine.koppelt@innoq.com>

RUN apk update

WORKDIR /app

ADD target/demo-0.0.1-SNAPSHOT.jar /app

EXPOSE 8080

CMD java -Dspring.profiles.active=docker -jar demo-0.0.1-SNAPSHOT.jar

